# compliance-pipelines

nötige config in der pom.xml:

```
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-checkstyle-plugin</artifactId>
        <version>3.1.0</version>
        <configuration>
          <configLocation>google_checks.xml</configLocation>
          <consoleOutput>true</consoleOutput>
          <encoding>UTF-8</encoding>
          <failsOnError>true</failsOnError>
          <includeTestSourceDirectory>true</includeTestSourceDirectory>
          <violationSeverity>warning</violationSeverity>
        </configuration>
        <dependencies>
          <dependency>
            <groupId>com.puppycrawl.tools</groupId>
            <artifactId>checkstyle</artifactId>
            <version>8.26</version>
          </dependency>
        </dependencies>
      </plugin>
```

TODO: custom  pom.xml für checkstyle erstellen und im Checkstyle Job laden.  Siehe:  
https://stackoverflow.com/questions/19682455/how-to-externalise-the-checkstyle-config-for-maven-checkstyle-plugin


```
      <plugin>
        <groupId>org.owasp</groupId>
        <artifactId>dependency-check-maven</artifactId>
        <version>5.2.4</version>
        <configuration>
          <suppressionFiles>
            <suppressionFile>${basedir}/.owasp/project-suppressions.xml</suppressionFile>
            <suppressionFile>${basedir}/.owasp/temporary-suppressions.xml</suppressionFile>
          </suppressionFiles>
          <failBuildOnAnyVulnerability>true</failBuildOnAnyVulnerability>
          <formats>
            <format>JUNIT</format>
            <format>HTML</format>
          </formats>
          <dataDirectory>/mnt/</dataDirectory>
        </configuration>
      </plugin>
```
